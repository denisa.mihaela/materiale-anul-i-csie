/*
Suma elementelor de pe cele 2 diagonale unei matrici patratice
*/

#include <stdio.h>

using namespace std;

int main()
{
    int i, j, n;
    int x[50][50];
    printf("Introduceti dimensiunea matricii: ");
    scanf("%d", &n);
    for (i = 0; i < n; i++)
    {
        for (j = 0; j < n; j++)
        {
            printf("Introduceti elementul de pe pozitia (%d,%d): ", i + 1, j + 1);
            scanf("%d", &x[i][j]);
        }
    }

    int s1 = 0, s2 = 0;

    for (i = 0; i < n; i++)
    {
        s1 += x[i][i];
        s2 += x[i][n - i - 1];
    }

    printf("Suma de pe diagonala principala = %d \n", s1);

    printf("Suma de pe diagonala secundara = %d \n", s2);

    return 0;
}
