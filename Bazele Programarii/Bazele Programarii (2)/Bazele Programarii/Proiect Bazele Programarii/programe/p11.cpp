/*
media aritmetica a elementelor impare dintr-un vector
*/

#include <stdio.h>
#include <math.h>

using namespace std;

int main()
{
    int i, n, k = 0, s = 0;
    int x[50];
    printf("Introduceti dimensiunea vectorului: ");
    scanf("%d", &n);
    for (i = 0; i < n; i++)
    {
        printf("Introduceti elementul de pe pozitia (%d): ", i + 1);
        scanf("%d", &x[i]);
    }

    for (i = 0; i < n; i++)
    {
        if (x[i] % 2 == 1)
        {
            k++;
            s += x[i];
        }
    }

    printf("Media aritmetica a  elementelor impare este: %.2f", round(s / k));

    return 0;
}

