/*
suma n^2 recursiv doar a numerelor impare (1, 3, 5 ...)
*/

#include <stdio.h>

using namespace std;

float suma(int n)
{
    if (n <= 1) return 1;

    float s = (n % 2 != 0 ? n * n : 0) + suma(n - 1);

    return s;
}

int main()
{
    int n;
    float s = 0;

    printf("Introduceti n: ");
    scanf("%d", &n);

    s = suma(n);

    printf("Suma este: %.2f \n", s);
}
