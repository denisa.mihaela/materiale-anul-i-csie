/*
Determinantul unei matrici
*/

#include <stdio.h>

using namespace std;

int x[50][50];

int det(int cx, int cy, int size)
{
    if (size == 1)
    {
        return x[cx][cy];
    }
    else if (size == 2)
    {
        return x[cx][cy] * x[cx + 1][cy + 1] - x[cx][cy + 1] * x[cx + 1][cy];
    }
    else if (size == 3)
    {
        return  x[cx][cy] * x[cx + 1][cy + 1] * x[cx + 2][cy + 2] +
                x[cx + 1][cy] * x[cx + 2][cy + 1] * x[cx][cy + 2] +
                x[cx][cy + 1] * x[cx + 1][cy + 2] * x[cx + 2][cy] -
                x[cx + 2][cy] * x[cx + 1][cy + 1] * x[cx][cy + 2] -
                x[cx + 1][cy + 2] * x[cx + 2][cy + 1] * x[cx][cy] -
                x[cx][cy + 1] * x[cx + 1][cy] * x[cx + 2][cy + 2];
    }
}

int main()
{
    int i, j, n;
    do
    {
        printf("Introduceti dimensiunea matricii (maxim 3): ");
        scanf("%d", &n);

    } while (n < 1 || n > 3);

    for (i = 0; i < n; i++)
    {
        for (j = 0; j < n; j++)
        {
            printf("Introduceti elementul de pe pozitia (%d,%d): ", i + 1, j + 1);
            scanf("%d", &x[i][j]);
        }
    }

    printf("Determinantul matricii = %d \n", det(0, 0, n));

    return 0;
}
