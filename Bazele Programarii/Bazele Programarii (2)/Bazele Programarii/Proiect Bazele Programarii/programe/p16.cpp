/*
intersectia a doua multimi
*/

#include <stdio.h>

using namespace std;

int exists(int t[50], int size, int el)
{
    for (int i = 0; i < size; i++)
    {
        if (t[i] == el) return 1;
    }
    return 0;
}

int main()
{
    int i, n, m, k;
    int x[50], y[50], z[50];
    printf("Introduceti dimensiunea primului vector: ");
    scanf("%d", &n);
    printf("Introduceti dimensiunea celui de-al doilea vector: ");
    scanf("%d", &m);
    for (i = 0; i < n; i++)
    {
        printf("Introduceti elementul de pe pozitia (%d) pentru primul vector: ", i + 1);
        scanf("%d", &x[i]);
    }

    for (i = 0; i < m; i++)
    {
        printf("Introduceti elementul de pe pozitia (%d) pentru al doilea vector: ", i + 1);
        scanf("%d", &y[i]);
    }

    k = 0;
    for (i = 0; i < n; i++)
    {
        if (!exists(z, k, x[i]) && exists(y, m, x[i]))
        {
            z[k++] = x[i];
        }
    }

    printf("Intersectia este: ");
    for (i = 0; i < k; i++)
    {
        printf("%d ", z[i]);
    }

    return 0;
}


