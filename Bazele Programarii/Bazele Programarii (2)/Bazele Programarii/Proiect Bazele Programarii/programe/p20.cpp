/*
sa se construiasca o matrice in spirala
*/

#include <stdio.h>

int main()
{
    int i = 0, j = -1, k, p, n, c = 0, dx, dy, count;
    int x[50][50];

    printf("Introduceti dimensiunea matricii: ");
    scanf("%d", &n);

    count = n;

    for (k = 1; k <= 2 * n - 1; k++)
    {
        dx = k % 4 == 1 ? 1 : (k % 4 == 3 ? -1 : 0);
        dy = k % 4 == 2 ? 1 : (k % 4 == 0 ? -1 : 0);
        if (k % 2 == 0) count--;

        for (p = 0; p < count; p++)
        {
            i += dy;
            j += dx;
            x[i][j] = ++c;
        }
    }

    printf("Matricea rezultata este: \n");

    for (i = 0; i < n; i++)
    {
        for (j = 0; j < n; j++)
        {
            printf("%d\t", x[i][j]);
        }
        printf("\n");
    }

}

