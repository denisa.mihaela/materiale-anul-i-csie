/*
Suma elementelor maxime de pe fiecare linie (daca exista de cel putin 2 ori pe aceeasi linie, sa se adune toate maximurile)
*/

#include <stdio.h>

using namespace std;

int main()
{
    int i, j, m, n;
    int x[50][50];
    printf("Introduceti numarul de linii si de coloane: ");
    scanf("%d %d", &m, &n);
    for (i = 0; i < m; i++)
    {
        for (j = 0; j < n; j++)
        {
            printf("Introduceti elementul de pe pozitia (%d,%d): ", i + 1, j + 1);
            scanf("%d", &x[i][j]);
        }
    }

    int s = 0;
    int max;

    for (i = 0; i < m; i++)
    {
        max = x[i][0];
        for (j = 1; j < n; j++)
        {
            if (x[i][j] > max) max = x[i][j];
        }
        for (j = 0; j < n; j++)
        {
            s += (max == x[i][j]) ? max : 0;
        }
    }

    printf("Suma elementelor maxime de pe fiecare linie = %d \n", s);

    return 0;
}
