/*
gasirea primelor n numere prime
*/

#include <stdio.h>
#include <math.h>

using namespace std;

int main()
{
    int n;

    printf("Introduceti numarul n: ");
    scanf("%d", &n);

    int x[50], k = 0, p = 0, i = 0, prim;
    while (p < n)
    {
        i++;
        prim = 1;
        for (k = 2; k <= ceil(i / 2); k++)
        {
            if (i % k == 0) prim = 0;
        }
        if (prim) x[p++] = i;
    }

    printf("Numerele prime gasite sunt: \n");
    for (i = 0; i < n; i++)
    {
        printf("%d ", x[i]);
    }

    return 0;
}

