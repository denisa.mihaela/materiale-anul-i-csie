/*
sa se inverseze un string si sa se transformele toate literele in litere mari
*/

#include <stdio.h>
#include <string.h>
#include <math.h>

using namespace std;

int main()
{
    char s[100], t;
    int i, k = 0, c, n;

    printf("Introduceti string-ul: ");
    scanf("%[^\n]", &s);

    n = strlen(s);

    for (i = 0; i < ceil(n / 2); i++)
    {
        t = s[i];
        s[i] = s[n - i - 1];
        s[n - i - 1] = t;
    }

    for (i = 0; i < n; i++)
    {
        c = (int) s[i];
        if (c >= 97 && c <= 122)
        {
            s[i] = (char) (c - 32);
        }
    }

    printf("Numele resultat = %s \n", s);

    return 0;
}

