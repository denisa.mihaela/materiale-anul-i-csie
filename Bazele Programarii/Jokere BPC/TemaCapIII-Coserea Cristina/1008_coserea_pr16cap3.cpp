#include<stdio.h>
#include<conio.h>
#include<stdlib.h>
void main()
{
	int n, m, i, j, a[20][20], max, soc[20], s, nr;
	//citirea de la tastatura a elementelor matricei
	printf("\nNumarul de societati= "); scanf_s("%d", &m);
	printf("\nNumarul de ani= "); scanf_s("%d", &n);
	for (i = 0; i < m; i++)
		for (j = 0; j < n; j++)
		{
			printf("a[%d][%d]=", i, j); scanf_s("%d", &a[i][j]);
		}
	//determinarea profitului maxim
	max = a[0][0];
	for (i = 0; i < m; i++)
		for (j = 0; j < n; j++)
			if (a[i][j] > max) max = a[i][j];
	nr = 0;
	for (i = 0; i < m; i++)
	{
		j = 0;
		while ((j < n) && (max != a[i][j])) j++;
		if (j < n) {
			soc[nr] = i; nr++;
		}
	}
	printf("\nSocietatile care au inregistrat profit net maxim %d sunt: \n", max);
	for (i = 0; i < nr; i++) printf("%d", soc[i] + 1);
	_getch();
}