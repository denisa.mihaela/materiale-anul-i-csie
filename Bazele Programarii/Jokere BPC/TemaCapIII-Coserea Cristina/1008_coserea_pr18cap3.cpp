#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <conio.h>
#include <stdlib.h>
void main()
{
	int i, j, k, m, n, a[20][20], nr, vect[45][20];
	printf("Nr linii= "); scanf("%d", &m);
	printf("Nr coloane= "); scanf("%d", &n);
	for (i = 0; i < m; i++)
		for (j = 0; j < n; j++)
		{
			printf("a[%d][%d]= ", i, j); scanf("%d", &a[i][j]);

		}
	printf("Matricea initiala:\n");
	for (i = 0; i < m; i++) {
		for (j = 0; j < n; j++)
			printf("%3d", a[i][j]); printf("\n");
	}
	nr = 0;
	for (i = 0; i < m - 1; i++)
		for (j = i + 1; j < m; j++)
		{
			for (k = 0; k < n; k++)
				vect[nr][k] = a[i][k] * a[j][k];
			nr++;
		}
	printf("\nMatricea care contine produsele vectoriale este: \n");
	for (i = 0; i < nr; i++){
		for (j = 0; j < n; j++) printf("%4d", vect[i][j]);
		printf("\n");
	}
	_getch();
}