#define _CRT_SECURE_NO_WARNINGS
#include<stdio.h>
#include<conio.h>
#include<stdlib.h>
void main()
{
	int i, j, m, n, rl, gasit, a[20][20];
	printf("Nr de linii= "); scanf("%d", &m);
	printf("Nr de coloane= "); scanf("%d", &n);
	for (i = 0; i < m; i++)
		for (j = 0; j < n; j++)
		{
			printf("a[%d][%d]= ", i, j); scanf("%d", &a[i][j]);
		}
	gasit = 0;
	for (j = 0; j < n; j++)
	{
		rl = a[1][j] - a[0][j];
		for (i = 1; i<m && a[i][j] - a[i - 1][j] == rl; i++);
		if (i == m)
		{
			printf("Coloana %d: ", j + 1); gasit = 1;
			for (i = 0; i<m; i++)
				printf("%d", a[i][j]);
			printf("este in progresie aritmetica\n ");
		}
	}
	if (!gasit) printf("Nu exista coloane care au elementele in progresie aritmetica!");
	_getch();
}
