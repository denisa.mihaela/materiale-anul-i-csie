#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <conio.h>
void main()
{
	float  a[10][10], b[10][10];
	int i, j, n;
	printf("nr linii/coloane= "); scanf("%d", &n);
	for (i = 0; i < n; i++)
		for (j = 0; j < n; j++)
		{
			printf("a[%d][%d]= ", i, j); scanf("%f", &a[i][j]);
		}
	for (i = 0; i < n; i++)
		for (j = 0; j < n; j++)
		{
			b[i][j] = a[j][i];
		}
	printf("Matricea initiala este:\n");
	for (i = 0; i < n; i++)
	{
		for (j = 0; j < n; j++)
			printf("%.2f\t", a[i][j]);
		printf("\n");
	}
	printf("Matrice transpusa este:\n");
	for (i = 0; i < n; i++)
	{
		for (j = 0; j < n; j++)
			printf("%.2f\t", b[i][j]);
		printf("\n");
	}
	_getch();
}