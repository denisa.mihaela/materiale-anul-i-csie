#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <conio.h>
void main()
{
	float a[10][10], b[10][10];
	int i, j, m, n;
	printf("m="); scanf("%d", &m);
	printf("n= "); scanf("%d", &n);
	for (i = 0; i < m; i++)
		for (j = 0; j < n; j++)
		{
			printf("a[%d][%d]= ", i, j); scanf("%f", &a[i][j]);
		}
	for (i = 0; i < m; i++)
		for (j = 0; j < n; j++)
			if (i < j) b[i][j] = a[i][j];
			else b[i][j] = (a[i][0] + a[i][n - 1] + a[0][j] + a[m - 1][j]) / 4;
			printf("Matricea rezulta este: \n");
			for (i = 0; i < m; i++)
			{
				for (j = 0; j < n; j++) printf("%5.2f", b[i][j]);
				printf("\n");
			}
			_getch();
}