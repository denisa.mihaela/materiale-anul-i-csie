#define _CRT_SECURE_NO_WARNINGS
#include<stdio.h>
#include<malloc.h>
#include<conio.h>
#include<string.h>
typedef struct {
	char nume[30];
	float medie;
} Student;

void main()
{
	int n, i, j, max; Student *st, aux;
	printf("Introduceti numarul de studenti bursieri: "); scanf("%d", &n);
	st = (Student*)malloc(n*sizeof(Student));
	for (i = 0; i < n; i++)
	{
		printf("Studentul %d: \n", i + 1);
		fflush(stdin);
		printf("Numele:"); gets(st[i].nume);
		printf("Media:"); scanf("%f", &st[i].medie);
	}
	for (i = 0; i < n - 1; i++)
	{
		for (max = i, j = i + 1; j < n; j++)
			if ((st[max].medie < st[j].medie) || ((st[max].medie == st[j].medie) && (_stricmp(st[max].nume, st[j].nume)>0))) max = j;
		if (max != i)
		{aux = st[i]; st[i] = st[max]; st[max] = aux;
	}
}
printf("Studentii bursieri sunt:\n");
for (i = 0; i < n; i++)
	printf("%d. %-30s: %5.2f\n", i + 1, st[i].nume, st[i].medie);
	free(st);
	_getch();
	}