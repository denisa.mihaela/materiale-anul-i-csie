#define _CRT_SECURE_NO_WARNINGS
#include<stdio.h>
#include<malloc.h>
#include<conio.h>
void main()
{
	int m, n, i, j, nr = 0;
	float **a, val;
	printf("numarul de linii: "); scanf("%d", &m);
	printf("numarul de coloane: "); scanf("%d", &n);
	a = (float**)malloc(m*sizeof(float*));
	for (i = 0; i < m; i++)
		a[i] = (float*)malloc(n*sizeof(float));
	for (i = 0; i < m; i++)
	for (j = 0; j < n; j++)
	{
		printf("a[%d][%d]=", i, j);
		scanf("%f", &(*(*(a+i)+j)));
	}
	printf("val="); scanf("%f", &val);
	printf("Matricea este\n");
	for (i = 0; i < m; i++)
	{
		for (j = 0; j < n; j++)
			printf("%5.2f", *(*(a + i) + j));
		printf("\n");
	}
	for (i = 0; i < m; i++)
		for (j = 0; j < n; j++)
			if ((*(*(a + i) + j)) == val) nr = nr + 1;
	printf("Valoarea cautata apare de = %d ori", nr);
	for (i = 0; i < m; i++)
	free(*(*(a + i) + j));
	free(a);
	_getch();
}