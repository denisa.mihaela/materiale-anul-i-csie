#define _CRT_SECURE_NO_WARNINGS
#include<stdio.h>
#include<malloc.h>
#include<conio.h>
void main()
{
	int m, n, i, j, nr = 0,ok;
	float **a;
	printf("numarul de linii: "); scanf("%d", &m);
	printf("numarul de coloane: "); scanf("%d", &n);
	a = (float**)malloc(m*sizeof(float*));
	for (i = 0; i < m; i++)
		a[i] = (float*)malloc(n*sizeof(float));
	for (i = 0; i < m; i++)
		for (j = 0; j < n; j++)
		{
			printf("a[%d][%d]=", i, j);
			scanf("%f", &(*(*(a + i) + j)));
		}
	printf("Matricea este\n");
	for (i = 0; i < m; i++)
	{
		for (j = 0; j < n; j++)
			printf("%5.2f", *(*(a + i) + j));
		printf("\n");
	}
	for (i = 0; i < m; i++)
	{
		ok = 0;
		for (j = 0; j < n - 1; j++)
			if ((*(*(a + i) + j)) < (*(*(a + i) + j + 1))) ok = ok + 1;
		if (ok == n - 1) nr = nr + 1;
	}
	printf("Numarul liniilor cu elementele in ordine crescatoare este = %d", nr);
	for (i = 0; i < m; i++)
		free(*(*(a + i) + j));
	free(a);
	_getch();
}