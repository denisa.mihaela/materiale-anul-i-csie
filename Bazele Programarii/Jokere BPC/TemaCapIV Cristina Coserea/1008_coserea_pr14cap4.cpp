#define _CRT_SECURE_NO_WARNINGS
#include<stdio.h>
#include<malloc.h>
#include<conio.h>
void main()
{
	int m, n, p, i, j, k;
	float **a, *b, *c;
	printf("numarul de linii: "); scanf("%d", &m);
	printf("numarul de coloane: "); scanf("%d", &n);
	a = (float**)malloc(m*sizeof(float*));
	for (i = 0; i < m; i++)
		a[i] = (float*)malloc(n*sizeof(float));
	for (i = 0; i < m; i++)
		for (j = 0; j < n; j++)
		{
			printf("a[%d][%d]=", i, j);
			scanf("%f", &(*(*(a + i) + j)));
		}
	b = (float*)malloc(n*sizeof(float));
	for (i = 0; i < n; i++)
	{
		printf("b[%d]=", i);
		scanf("%f", &(*(b + i)));
	}
	c = (float*)malloc(n*sizeof(float));
	for (i = 0; i < m; i++)
		*(c + i) = 0;
	for (i = 0; i < m; i++)
		for (k = 0; k < n; k++)
			*(c + i) = *(c + i) + (*(*(a + i) + k))*(*(b + k));
	for (i = 0; i < m; i++)
		printf("c[%d]=%f\n", i, *(c + i));
	free(c);
	free(b);
	for (i = 0; i < m; i++)
		free(*(a + i));
	free(a);
	_getch();
}