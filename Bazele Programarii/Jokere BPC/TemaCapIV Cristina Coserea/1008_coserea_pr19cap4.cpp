#define _CRT_SECURE_NO_WARNINGS
#include<stdio.h>
#include<malloc.h>
#include<conio.h>
void main()
{
	int m, n, p, i, j, k;
	float **a, min;
	printf("Dimensiunea matricei ="); scanf("%d", &m);
	a = (float**)malloc(m*sizeof(float*));
	for (i = 0; i < m; i++)
		*(a + i) = (float*)malloc(m*sizeof(float));
	for (i = 0; i < m; i++)
		for (j = 0; j < m; j++)
		{
			printf("a[%d][%d]=", i, j);
			scanf("%f", *(a + i) + j);
		}
	min = *(*(a + 1) + m-1);
	for (i = 1; i <= m - 1; i++)
		for (j = m-1; (j >i) && (j >m- i-1); j--)
			if ((*(*(a + i) + j)) < min) min = *(*(a + i) + j);
	printf("minimul este: %5.2f", min);
	for (i = 0; i < m; i++)
		free(*(a + i));
	free(a);
	_getch();
}