#define _CRT_SECURE_NO_WARNINGS
#include<stdio.h>
#include<malloc.h>
#include<conio.h>
void main()
{
	float *x, a; int n, i, *poz, nrpoz;
	printf("\n Introduceti dimensiunea vectorului= "); scanf("%d", &n);
	x = (float*)malloc(n*sizeof(float));
	for (i = 0; i < n; i++)
		{
			printf("x[%d]= ", i); scanf("%f", x+i);
		}
	printf("Valoarea cautata este= "); scanf("%f", &a);
	poz = (int*)malloc(n*sizeof(int));
	nrpoz = 0;
	for (i = 0; i < n; i++)
		if (x[i] == a) poz[nrpoz++] = i;
	if (!nrpoz) printf("Valoarea %5.2f nu exista in vector", a);
	else 
	{
		printf("valoarea %5.2f apare pe pozitiile:", a);
		for (i = 0; i<nrpoz; i++) printf("%d; ", poz[i]);
	}
	free(x);
	free(poz);
	_getch();
}