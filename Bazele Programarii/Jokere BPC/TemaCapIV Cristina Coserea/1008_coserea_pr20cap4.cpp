#define _CRT_SECURE_NO_WARNINGS
#include<stdio.h>
#include<malloc.h>
#include<conio.h>
void main()
{
	int m, n, p, i, j, k;
	float **a, *v;
	printf("Dimensiunea matricei ="); scanf("%d", &m);
	a = (float**)malloc(m*sizeof(float*));
	for (i = 0; i < m; i++)
		*(a + i) = (float*)malloc(m*sizeof(float));
	for (i = 0; i < m; i++)
		for (j = 0; j < m; j++)
		{
			printf("a[%d][%d]=", i, j);
			scanf("%f", *(a + i) + j);
		}
	v = (float*)malloc(m*sizeof(float));
	for (i = 0; i < m; i++)
		*(v + i) = (*(*(a + i) + i))*(*(*(a + i) + m - i - 1));
	printf("Vectorul este:\n");
	for (i = 0; i < m; i++)
		printf("%5.2f", *(v + i));
	for (i = 0; i < m; i++)
		free(*(a + i));
	free(a);
	free(v);
	_getch();
}