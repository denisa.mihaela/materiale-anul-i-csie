#define _CRT_SECURE_NO_WARNINGS
#include<stdio.h>
#include<malloc.h>
#include<conio.h>
void main()
{
	float *x, max; int n, i, *poz, nrpoz;
	printf("\n Introduceti dimensiunea vectorului, n= "); scanf("%d", &n);
	x = (float*)malloc(n*sizeof(float));
	for (i = 0; i < n; i++)
	{
		printf("x[%d]= ", i); scanf("%f", x + i);
	}
	poz = (int*)malloc(n*sizeof(int));
	max = x[0]; poz[0] = 0; nrpoz = 1;
	for (i = 0; i < n; i++)
		if (max < x[i]) {
			max = x[i]; poz[0] = i; nrpoz = 1;
		}
		else if (max == i) { poz[nrpoz] = i; nrpoz++; }
	printf("Maximul este %5.2f si apare pe pozitiile: ", max);
	for (i = 0; i<nrpoz; i++) printf("%d; ", poz[i]);
	free(x);
	free(poz);
	_getch();
	}