#define _CRT_SECURE_NO_WARNINGS
#include<stdio.h>
#include<malloc.h>
#include<conio.h>
void main()
{
	float *val, med, min, max;  int n, i, *poz, k;
	printf("Introduceti numarul de ani, n= "); scanf("%d", &n);
	val = (float*)malloc(n*sizeof(float));
	for (i = 0; i < n; i++)
	{
		printf("val[%d]= ", i+1); scanf("%f", val + i);
	}
	for (med = min = max = val[0], i = 1; i < n; med += val[i], i++)
		if (val[i] < min) min = val[i];
		else if (val[i] > med) max=val[i];
		med /= n;
		k = 0;
		poz = (int*)malloc(n*sizeof(int));
		for (i = 0; i < n; i++)
			if (val[i]>med) poz[k++] = i;

		printf("Amplitudinea vanzarilor este %5.2f\n", max - min);
		printf("Anii in care s-a depasit media este de %5.2f sunt: ", med);
		for (i = 0; i < k; i++) printf("%d", poz[i] + 1);
		free(val); free(poz);
	_getch();
}