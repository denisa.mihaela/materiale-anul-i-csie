#define _CRT_SECURE_NO_WARNINGS
#include<stdio.h>
#include<malloc.h>
#include<conio.h>
typedef struct {
	int nr;
	char nume[30];
	char n, note[15];
} Student;
void main()
{
	int n, i, j, *poz, k; Student *st;
	printf("Introduceti numarul de studenti din grupa: "); scanf("%d", &n);
	st = (Student*)malloc(n*sizeof(Student));
	for (i = 0; i < n; i++)
	{
		printf("Studentul %d: \nNumarul matricol:", i + 1); scanf("%d", &st[i].nr);
		fflush(stdin);
		printf("Numele:"); gets(st[i].nume);
		printf("Nr. note:"); scanf("%d", &st[i].n);
		for (j = 0; j < st[i].n; j++)
		{
			printf("nota[%d]= ", j);
			scanf("%d", &st[i].note[j]);
		}
	}
	poz = (int*)malloc(n*sizeof(int));
	k = 0;
	for (i = 0; i < n;i++)
	{
		j = 0;
		while ((j < st[i].n) && (st[i].note[j] >= 5))j++;
		if (j == st[i].n) poz[k++] = i;
	}
	if (!k) printf("Nu exista studenti integralisti in aceasta grupa");
	else {
		printf("Studenti integralisti sunt:");
		for (i = 0; i < k; i++) printf("\n%d. %-s", i + 1, st[poz[i]].nume);
	}
			free(st); free(poz);
			_getch();
}