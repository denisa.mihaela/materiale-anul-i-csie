#define _CRT_SECURE_NO_WARNINGS
#include<stdio.h>
#include<malloc.h>
#include<conio.h>
typedef struct {
	char nume[30], note[15];
} Student;
void main()
{
	int n, m, i, j; Student *st; float *med;
	printf("Introduceti numarul de discipline: "); scanf("%d", &m);
	printf("Introduceti numarul de studenti din grupa: "); scanf("%d", &n);
	st = (Student*)malloc(n*sizeof(Student));
	for (i = 0; i < n; i++)
	{
		printf("Studentul %d: \n", i + 1);
		fflush(stdin);
		printf("Numele:"); gets(st[i].nume);
		for (j = 0; j < m; j++)
		{
			printf("nota[%d]=", j + 1);
			scanf("%d", &st[i].note[j]);
		}
	}
	med = (float*)malloc(m*sizeof(float));
	for (j = 0; j < m; j++)
	{
		med[j] = 0;
		for(i = 0; i < n; i++)
			med[j] += st[i].note[j];
		med[j] /= n;
	}
	printf("Mediile sunt:\n");
	for (j = 0; j < m; j++)
		printf("Disciplina %d = %5.2f\n", j + 1, med[j]);
	free(st); free(med);
	_getch();
}