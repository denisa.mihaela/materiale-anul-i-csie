#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <conio.h>
typedef struct {
	int nr;
	struct PRODUS {
		int codp;
		float cant, pret;
	}p[50];
}ZI;
void citire(float x[], int *n)
{
	int i;
	printf("n= "); scanf("%d", n);
	for (i = 0; i < *n; i++)
	{
		printf("x[%d]= ", i); scanf("%f", &x[i]);
	}
}
void zile(ZI v[], int n, double *max, int *k, int poz[])
{
	int i, j; double val;
	*max = -1;
	for (i = 0; i < n; i++) {
		val = 0;
		for (j = 0; j < v[i].nr; j++)
			val += v[i].p[j].cant*v[i].p[j].pret;
		if (val > *max) {
			*max = val; poz[0] = i; *k = 1;
		}
		else if (val == *max) {
			poz[*k] = i; (*k)++;
		}
	}
}

void main()
{
	int n, i, j; ZI v[50];
	printf("Introduceti numarul de zile:"); scanf("%d", &n);
	for (i = 0; i < n; i++){
		printf("Ziua %d:\n", i + 1);
		printf("Numarul de produse:"); scanf("%d", &v[i].nr);
		for (j = 0; j < v[i].nr; j++){
			printf("Cod produs:"); scanf("%d", &v[i].p[j].codp);
			printf("Cantitatea vanduta:"); scanf("%f", &v[i].p[j].cant);
			printf("Pret unitar:"); scanf("%f", &v[i].p[j].pret);
		}
	}
	int k, poz[50]; double max;
	zile(v, n, &max, &k, poz);
	printf("Zilele in care s-a inregistrat valoarea maxima de %5.21f sunt:", max);
	for (i = 0; i < k; i++) printf("%d;", poz[i]);
	_getch();
}
