#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <conio.h>
void citire_mat(float a[][100], int*m, int*n)
{
	int i, j;
	printf("numarul de linii="); scanf("%d", m);
	printf("numarul de coloane="); scanf("%d", n);
	for (i = 0; i < *m; i++)
		for (j = 0; j < *n; j++)
		{
			printf("[%d][%d]=", i, j);
			scanf("%f", &a[i][j]);
		}
}
void afisare_mat(float a[][100], int m, int n)
{
	int i, j;
	printf("Matricea este\n");
	for (i = 0; i < m; i++)
	{
		for (j = 0; j < n; j++)
			printf("%5.2f", a[i][j]);
		printf("\n");
	}
}
int nr_lin_const_var2(float a[][100], int m, int n)
{
	int i, j, ok, nr = 0;
	for (i = 0; i < m; i++)
	{
		ok = 1;
		for (j = 0; j < n; j++)
			if (a[i][0] != a[i][j]) ok = 0;
		if (ok == 1) nr = nr + 1;
	}
	return nr;
}
void main()
{
	int m, n, i, j, numar1, numar2;
	float a[100][100];

	citire_mat(a, &m, &n);
	afisare_mat(a, m, n);
	numar2 = nr_lin_const_var2(a, m, n);
	printf("(varianta 2) numarul liniilor cu elemente constante este = %d", numar2);
	_getch();
}