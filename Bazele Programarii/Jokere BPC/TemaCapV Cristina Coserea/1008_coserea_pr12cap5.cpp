#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <conio.h>
void citire_mat(float a[][100], int*m, int*n)
{
	int i, j;
	printf("numarul de linii="); scanf("%d", m);
	printf("numarul de coloane="); scanf("%d", n);
	for (i = 0; i < *m; i++)
		for (j = 0; j < *n; j++)
		{
			printf("[%d][%d]=", i, j);
			scanf("%f", &a[i][j]);
		}
}
void lin_const(float a[][100], int m, int n, int v[100], int *nr)
{
	int i, j, ok;
	*nr = 0;
	for (i = 0; i < m; i++)
	{
		ok = 1;
		for (j = 0; j < n; j++)
			if (a[i][0] != a[i][j]) ok = 0;
		if (ok == 1) {
			v[(*nr)] = i;
			(*nr) = (*nr) + 1;
		}
	}
}
void main()
{
	float a[100][100];
	int m, n, k, v[100];
	citire_mat(a, &m, &n);
	lin_const(a, m, n, v, &k);
	printf("Abonatii care au inregistrat valori constante sunt in numar de %d", k);
	_getch();
}