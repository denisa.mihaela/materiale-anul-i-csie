#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <conio.h>
void citire_mat(float a[][100], int*m, int*n)
{
	int i, j;
	printf("numarul de linii="); scanf("%d", m);
	printf("numarul de coloane="); scanf("%d", n);
	for (i = 0; i < *m; i++)
		for (j = 0; j < *n; j++)
		{
			printf("[%d][%d]=", i, j);
			scanf("%f", &a[i][j]);
		}
}
int col_nule(float a[][100], int m, int n, int t[100])
{
	int i, j, ok;
	int p = 0;
	for (j = 0; j < n; j++)
	{
		ok = 1;
		for (i = 0; i < m; i++)
			if (a[i][0] != 0) ok = 0;
		if (ok == 1)
		{
			t[p] = j;
			p = p + 1;
		}
	}
	return p;
}
void main()
{
	int m, n, nr_c, t[100];
	float a[100][100];
	citire_mat(a, &m, &n);
	nr_c = col_nule(a, m, n, t);
	printf("zilele in care s-au inregistrat consumuri nule sunt %d", nr_c);
	_getch();
}