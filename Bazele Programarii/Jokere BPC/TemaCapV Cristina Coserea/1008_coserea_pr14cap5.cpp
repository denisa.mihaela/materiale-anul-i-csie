#define _CRT_SECURE_NO_WARNINGS
#include<conio.h>
#include<stdio.h>
int produs(unsigned  a[][100], unsigned  b[][100], int ca, int lb, unsigned  c[][100])
{
	int i, j, k, er, m, n, p;
	if (ca != lb)
		er = 1;
	else
	{
		er = 0;
		for (i = 0; i<ca; i++)
			for (j = 0; j< p; j++)
			{
				c[i][j] = 0;
				for (k = 0; k<lb; k++)
					c[i][j] = c[i][j] || (a[i][k] && b[k][j]);
			}
	}
	return er;
}
void citire_mat(unsigned  a[][100], int *m, int *n)
{
	int i, j;
	printf("numarul de linii =");
	scanf("%d", m);
	printf("numarul de coloane =");
	scanf("%d", n);
	for (i = 0; i<*m; i++)
		for (j = 0; j<*n; j++)
		{
			printf("[%d][%d]=", i, j);
			scanf("%u", &a[i][j]);
		}
}
void afisare_mat(unsigned  a[][100], int m, int n)
{
	int i, j;
	printf("Matricea este\n");
	for (i = 0; i<m; i++)
	{
		for (j = 0; j<n; j++)
			printf("%u ", a[i][j]);
		printf("\n");
	}
}
void main()
{
	int la, ca, lb, cb, lc, clc, cc, er, m, n;
	unsigned a[100][100], b[100][100], c[100][100];
	citire_mat(a, &la, &ca);
	afisare_mat(a, la, ca);
	citire_mat(b, &lb, &cb);
	afisare_mat(b, lb, cb);
	er = produs(a, b, ca, lb, c);
	if (er == 0)
		afisare_mat(c, ca, lb);
	else
		printf("Matricile nu se pot inmulti");
	_getch();
}