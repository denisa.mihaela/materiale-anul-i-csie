#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <conio.h>
void citire_mat(float a[][100], int*m, int*n)
{
	int i, j, p;
	printf("m="); scanf("%d", m);
	printf("n="); scanf("%d", n);
	for (i = 0; i < *m; i++)
		for (j = 0; j < *n; j++)
		{
			printf("a[%d][%d]=", i, j);
			scanf("%f", &a[i][j]);
		}
}
void ord_linii(float a[][100], int m, int n)
{
	int i, j, k;
	for (i = 0; i < m; i++)
		for (j = 0; j < n - 1; j++)
			for (k = j + 1; k < n; k++)
				if (a[i][j] > a[i][k])
				{
					float aux = a[i][j];
					a[i][j] = a[i][k];
					a[i][k] = aux;
				}
}
void main()
{
	int m, n; int i, j;
	float a[100][100];
	citire_mat(a, &m, &n);
	ord_linii(a, m, n); 
	for (i = 0; i < m; i++)
	{
		for (j = 0; j < n; j++)
			printf("%5.2f ", a[i][j]); printf("\n");
	}
	_getch();
}