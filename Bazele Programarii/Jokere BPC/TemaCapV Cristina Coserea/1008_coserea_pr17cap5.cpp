#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <conio.h>
void citire_mat(float a[][100], int*m, int*n)
{
	int i, j, p;
	printf("m="); scanf("%d", m);
	printf("n="); scanf("%d", n);
	for (i = 0; i < *m; i++)
		for (j = 0; j < *n; j++)
		{
			printf("a[%d][%d]=", i, j);
			scanf("%f", &a[i][j]);
		}
}
void consum_prog_arit(float a[][100], int m, int n, int v[100], int *nr)
{
	int i, j, ok;
	float r;
	*nr = 0;
	for (i = 0; i < m; i++)
	{
		ok = 1;
		r = a[i][1] - a[i][0];
		for (j = 0; j < n - 1; j++)
			if ((a[i][j + 1] - a[i][j]) != r) ok = 0;
		if (ok == 1)
		{
			v[(*nr)] = i;
			(*nr) = (*nr) + 1;
		}
	}
}
void main()
{
	int m, n, k, v[100];
	float a[100][100];
	citire_mat(a, &m, &n);
	consum_prog_arit(a, m, n, v, &k);
	printf("\nconsumuri in p.a pe intreaga perioada fabricarea a %d produse", k);
	_getch();
}