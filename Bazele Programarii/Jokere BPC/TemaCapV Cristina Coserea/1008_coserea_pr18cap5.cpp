#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <conio.h>
void citire_mat(float a[][100], int*m, int*n)
{
	int i, j, p;
	printf("m="); scanf("%d", m);
	printf("n="); scanf("%d", n);
	for (i = 0; i < *m; i++)
		for (j = 0; j < *n; j++)
		{
			printf("[%d][%d]=", i, j);
			scanf("%f", &a[i][j]);
		}
}
int const_descrescatoare(float a[][100], int m, int n, int v[100])
{int i, j, ok;
float r;
int nr = 0;
for (i = 0; i < m; i++)
{
	ok = 1;
	for (j = 0; j < n - 1; j++)
		if (a[i][j + 1] >= a[i][j]) ok = 0;
	if (ok == 1)
	{
		v[nr] = i;
		nr = nr + 1;
	}
}
return nr;
}
void main()
{
	int m, n, nr_l, t[100];
	float a[100][100];
	citire_mat(a, &m, &n);
	nr_l = const_descrescatoare(a, m, n, t);
	printf("\nconsumuri descrescatoare pe intreaga perioada fabricarea a %d produse", nr_l);
	_getch();
}