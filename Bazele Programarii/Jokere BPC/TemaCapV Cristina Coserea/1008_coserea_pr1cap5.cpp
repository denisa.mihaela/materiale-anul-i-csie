#define _CRT_SECURE_NO_WARNINGS
#include<stdio.h>
#include<conio.h>
//citire vector ce returneaza numar de elemente prin paramentru
void citire(float x[], int *n)
{
	int i;
	printf("n="); scanf("%d", n);
	for (i = 0; i < *n; i++)
	{
		printf("x[%d]=", i); scanf("%f", &x[i]);
	}
}
/*citire vector cu returnearea numarului de elemente prin numele subprogramului*/
int citire2(float x[])
{
	int i, n;
	printf("n="); scanf("%d", &n);
	for (i = 0; i < n; i++)
	{
		printf("x[%d]=", i); scanf("%f", &x[i]);
	}
	return n;
}
//afisarea elementelor dintr-un vector
void afisare(float x[], int n)
{
	int i;
	for (i = 0; i < n; i++) printf("%5.2f", x[i]);
}

/*varianta 1: subprogramul are ca parametri adresa vectorului,
numarul de elemente si adresa unde se va scrie media*/
void medie(float x[], int n, float *med)
{
	int i, k;
	k = 0; *med = 0;
	for (i = 0; i < n; i++)
		if (x[i] != 0) { *med += x[i]; k++; }
	if (k)*med /= k;
}
/*varianta 2: subprogramul are ca parametri adresa vectorului si 
numarul de elemente si returneaza, prin numele lui, media*/
float medie2(float x[], int n)
{
	int i, k = 0;
	float med = 0;
	for (i = 0; i < n; i++)
		if (x[i] != 0) { med += x[i]; k++; }
	if (k)med /= k;
	return med;
}
void main() {
float v[100], medv; int n;
citire(v, &n);
//sau varianta 2 de subprogram
//n=citire2(v);
medie(v, n, &medv);
//medv=medie2(v,n);
printf("Vanzarile zilnice sunt:");
afisare(v, n);
if (medv) printf("\nMedia vanzarilor este: %5.2f", medv);
else printf("\nSocietatea nu a inregistrat vanzari");
_getch();
}