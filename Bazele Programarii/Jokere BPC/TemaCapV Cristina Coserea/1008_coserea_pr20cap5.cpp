#define _CRT_SECURE_NO_WARNINGS
#include<stdio.h>
#include<conio.h>
void consum_mediu_zi(float a[][100], int m, int n, float v[100])
{
	int i, j, ok, p;
	for (j = 0; j<n; j++)
	{
		v[j] = 0;
		p = 0;
		for (i = 0; i<m; i++)
			if (a[i][j] != 0)
			{
				v[j] = v[j] + a[i][j];
				p = p + 1;
			}
		v[j] = v[j] / p;
		printf("%5.2f ", v[j]);
	}
}
void citire_mat(float a[][100], int *m, int *n)
{
	int i, j;
	printf("numarul de linii =");
	scanf("%d", m);
	printf("numarul de coloane =");
	scanf("%d", n);
	for (i = 0; i<*m; i++)
		for (j = 0; j<*n; j++)
		{
			printf("[%d][%d]=", i, j);
			scanf("%f", &a[i][j]);
		}
}
void afisare_mat(float a[][100], int m, int n)
{
	int i, j;
	printf("Matricea este\n");
	for (i = 0; i<m; i++)
	{
		for (j = 0; j<n; j++)
			printf("%5.2f ", a[i][j]);
		printf("\n");
	}
}
void main()
{
	int m, n;
	float a[100][100], x[100];
	citire_mat(a, &m, &n);
	afisare_mat(a, m, n);
	printf("\n");
	printf("\nConsumul mediu in fiecare zi: \n");
	consum_mediu_zi(a, m, n, x);
	_getch();
}