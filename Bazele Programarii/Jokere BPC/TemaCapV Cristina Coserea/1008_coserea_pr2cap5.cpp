#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <conio.h>
void citire(float x[], int *n)
{
	int i;
	printf("n= "); scanf("%d", n);
	for (i = 0; i < *n; i++)
	{
		printf("x[%d]= ", i); scanf("%f", &x[i]);
	}
}
void zile(float x[], int n, int poz[], int *k)
{
	int i;
	*k = 0;
	for (i = 0; i < n; i++)
		if (x[i] == 0) { poz[*k] = i; (*k)++; }
}
void afisare(int x[], int n)
{
	int i;
	for (i = 0; i < n; i++)
		printf("%d ", x[i]);
}
void main()
{
	float v[100];
	int n, nr, poz[100];
	citire(v, &n);

	zile(v, n, poz, &nr);
	if (!nr) printf("In toate zilele s-au inregistrat vanzari");
	else {
		printf("Zilele in care nu s-au inregistrat vanzari sunt: ");
		for (int i = 0; i < nr; i++)
			printf("%d;", poz[i] + 1);
	}
	_getch();
}