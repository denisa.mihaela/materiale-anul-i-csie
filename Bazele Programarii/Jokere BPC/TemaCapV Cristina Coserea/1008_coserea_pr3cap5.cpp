#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <conio.h>
void citire(float x[], int *n)
{
	int i;
	printf("n= "); scanf("%d", n);
	for (i = 0; i < *n; i++)
	{
		printf("x[%d]= ", i); scanf("%f", &x[i]);
	}
}
char crescator(float x[], int n)
{
	int i = 0; char ok;
	while (i < n - 1 && x[i] < x[i + 1]) i++;
	if (i < n - 1) ok = 0; else ok = 1;
	return ok;
}
void afisare(int x[], int n)
{
	int i;
	for (i = 0; i < n; i++)
		printf("%d ", x[i]);
}
void main()
{
	float v[100]; int n;
	citire(v, &n);
	char ok = crescator(v, n);
	if (ok)
		printf("S-au inregistrat creteri zilnice pe intreaga perioada");
	else
		printf("Nu s-au inregistrat cresterii zilnice pe intreaga perioada");
	_getch();
}
