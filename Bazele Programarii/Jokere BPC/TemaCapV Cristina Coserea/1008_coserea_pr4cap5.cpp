#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <conio.h>
unsigned cmmdc(unsigned a, unsigned b)
{
	unsigned int r, d = a, i = b;
	do {
		r = d%i;
		d = i;
		i = r;
	} while (r != 0);
	return d;
}
void citire(unsigned x[], int *n)
{
	int i;
	printf("n= "); scanf("%d", n);
	for (i = 0; i < *n; i++)
	{
		printf("x[%d]= ", i); scanf("%u", &x[i]);
	}
}
unsigned cmmdcn(unsigned x[], int n)
{
	int i; unsigned d;
	d = x[0]; i = 1;
	while ((d != 1) && (i < n)) {
		d = cmmdc(d, x[i]); i++;
	}
	return d;
}
void main()
{
	unsigned x[100]; int n;
	citire(x, &n);
	printf("\ncmmdc=%u", cmmdcn(x, n));
	_getch();
}