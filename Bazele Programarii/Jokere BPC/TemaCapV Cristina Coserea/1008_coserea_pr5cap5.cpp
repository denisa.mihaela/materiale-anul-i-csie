#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <conio.h>
unsigned cmmdc(unsigned a, unsigned b)
{
	unsigned int r, d = a, i = b;
	do {
		r = d%i;
		d = i;
		i = r;
	} while (r != 0);
	return d;
}
void citire(unsigned x[], int *n)
{
	int i;
	printf("n= "); scanf("%d", n);
	for (i = 0; i < *n; i++)
	{
		printf("x[%d]= ", i); scanf("%u", &x[i]);
	}
}
int nr_prime(unsigned x[], int n)
{
	int i, j, nr;
	nr = 0;
	for (i = 0; i < n - 1; i++)
		for (j = i + 1; j < n; j++)
			if (cmmdc(x[i], x[j]) == 1) nr++;
	return nr;
}
void main()
{
	unsigned x[100]; int n, i;
	citire(x, &n);
	printf("\n Numarul de perechi de numere prime intre ele este=%d \n", nr_prime(x, n));
	_getch();
}