#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <conio.h>
void citire(float x[], int *n)
{
	int i;
	printf("n= "); scanf("%d", n);
	for (i = 0; i < *n; i++)
	{
		printf("x[%d]= ", i); scanf("%f", &x[i]);
	}
}
void compactare(float x[], int *n)
{
	int i, j, k;
	for (i = 0; i < *n - 1; i++)
	{
		j = i + 1;
		while (j<*n)
			if (x[i] == x[j])
			{
				for (k = j; k < *n - 1; k++) x[k] = x[k + 1];
				(*n)--;
			}
			else j++;
	}
}
void main()
{
	float x[100]; int n;
	citire(x, &n);
	compactare(x, &n);
	for (int i = 0; i < n; i++)
		printf("x[%d]\t", i);
	_getch();
}