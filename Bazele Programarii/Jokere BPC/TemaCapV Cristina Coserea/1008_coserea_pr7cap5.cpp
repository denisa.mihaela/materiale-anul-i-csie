#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <conio.h>
typedef struct {
	int nr;
	struct PRODUS {
		int codp;
		float cant, pret;
	}p[50];
}ZI;
void citire(float x[], int *n)
{
	int i;
	printf("n= "); scanf("%d", n);
	for (i = 0; i < *n; i++)
	{
		printf("x[%d]= ", i); scanf("%f", &x[i]);
	}
}
double val_zi(ZI v[], int n, int z) {
	int i; double val = 0;
	if (z<1 || z>n) val = -1;
	else {
		for (i = 0; i < v[z - 1].nr; i++)
			val += v[z - 1].p[i].cant*v[z - 1].p[i].pret;
	}
	return val;
}
void main()
{
	int n, i, j; ZI v[50];
	printf("Introduceti numarul de zile:"); scanf("%d", &n);
	for (i = 0; i < n; i++){
		printf("Ziua %d:\n", i + 1);
		printf("Numarul de produse:"); scanf("%d", &v[i].nr);
		for (j = 0; j < v[i].nr; j++){
			printf("Cod produs:"); scanf("%d", &v[i].p[j].codp);
			printf("Cantitatea vanduta:"); scanf("%f", &v[i].p[j].cant);
			printf("Pret unitar:"); scanf("%f", &v[i].p[j].pret);
		}
	}
	int z;
	printf("Introduceti ziua:"); scanf("%d", &z);
	double val = val_zi(v, n, z);
	if (val == -1) printf("Nu exista ziua %d", n);
	else printf("Valoarea vanzarilor din ziua %d  este %5.2f", z, val);
	_getch();
}

