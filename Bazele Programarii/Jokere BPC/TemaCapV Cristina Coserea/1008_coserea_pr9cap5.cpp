#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <conio.h>
typedef struct {
	int nr;
	struct PRODUS {
		int codp;
		float cant, pret;
	}p[50];
}ZI;
void citire(float x[], int *n)
{
	int i;
	printf("n= "); scanf("%d", n);
	for (i = 0; i < *n; i++)
	{
		printf("x[%d]= ", i); scanf("%f", &x[i]);
	}
}
double val_totala(ZI v[], int n) {
	int i, j; double val = 0;
	for (i = 0; i < n; i++) {
		for (j = 0; j < v[i].nr; j++)				
			val += v[i].p[j].cant*v[i].p[j].pret;
	}
	return val;
}

void main()
{
	int n, i, j; ZI v[50];
	printf("Introduceti numarul de zile:"); scanf("%d", &n);
	for (i = 0; i < n; i++){
		printf("Ziua %d:\n", i + 1);
		printf("Numarul de produse:"); scanf("%d", &v[i].nr);
		for (j = 0; j < v[i].nr; j++){
			printf("Cod produs:"); scanf("%d", &v[i].p[j].codp);
			printf("Cantitatea vanduta:"); scanf("%f", &v[i].p[j].cant);
			printf("Pret unitar:"); scanf("%f", &v[i].p[j].pret);
		}
	}
	printf("Valoarea vanzarilor pe intreaga perioada este: %5.21f\n", val_totala(v, n));
	_getch();
}
