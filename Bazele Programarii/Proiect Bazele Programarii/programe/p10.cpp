/*
numarul de cifre dintr-un numar si suma lor
*/

#include <stdio.h>
#include <math.h>

using namespace std;

int main()
{
    int n, i = 0, k, s = 0, t = 0;

    printf("Introduceti numarul n: ");
    scanf("%d", &n);

    while (n > 0)
    {
        k = n % (int) pow10(t + 1);
        n -= k;
        s += ceil(k / pow10(t));
        t++;
    }

    printf("Sunt %d cifre si suma lor este %d \n", t, s);

    return 0;
}

