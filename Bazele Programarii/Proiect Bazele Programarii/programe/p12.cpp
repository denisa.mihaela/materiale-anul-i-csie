/*
produsul cartezian a 2 vectori
*/

#include <stdio.h>

using namespace std;

int main()
{
    int i, j, n, s = 0;
    int x[50], y[50];
    printf("Introduceti dimensiunea vectorilor: ");
    scanf("%d", &n);
    for (i = 0; i < n; i++)
    {
        printf("Introduceti elementul de pe pozitia (%d) pentru primul vector: ", i + 1);
        scanf("%d", &x[i]);
    }

    for (i = 0; i < n; i++)
    {
        printf("Introduceti elementul de pe pozitia (%d) pentru al doilea vector: ", i + 1);
        scanf("%d", &y[i]);
    }

    for (i = 0; i < n; i++)
    {
        s += x[i] * y[i];
    }

    printf("Produsul cartezian al celor 2 vectori este: %d ", s);

    return 0;
}

