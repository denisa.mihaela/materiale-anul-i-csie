/*
Inversa unei matrici
*/

#include <stdio.h>

using namespace std;

int main()
{
    int i, j, n;
    float x[50][50], y[50][50];
    printf("Introduceti dimensiunea matricii: ");
    scanf("%d", &n);
    for (i = 0; i < n; i++)
    {
        for (j = 0; j < n; j++)
        {
            printf("Introduceti elementul de pe pozitia (%d,%d): ", i + 1, j + 1);
            scanf("%f", &x[i][j]);
            y[i][j] = (i == j) ? 1 : 0;
        }
    }

    int ok = 0, p = 0;
    while (!ok && p < n)
    {
        for (i = 0; i < n; i++)
        {
            if (i != p)
            {
                for (j = 0; j < n; j++)
                {
                    if (j != p)
                    {
                        x[i][j] = (x[i][j] * x[p][p] - x[i][p] * x[p][j]) / x[p][p];
                    }
                    y[i][j] = (y[i][j] * x[p][p] - x[i][p] * y[p][j]) / x[p][p];
                }

                x[i][p] = 0;
            }
        }

        for (i = 0; i < n; i++)
        {
            if (i != p)
            {
                x[p][i] = x[p][i] / x[p][p];
            }
            y[p][i] = y[p][i] / x[p][p];
        }

        x[p][p] = 1;

        p++;
    }

    int t;

    printf("Matricea inversa este: \n");

    for (i = 0; i < n; i++)
    {
        for (j = 0; j < n; j++)
        {
            printf("%.2f\t", y[i][j]);
        }
        printf("\n");
    }

    return 0;
}
