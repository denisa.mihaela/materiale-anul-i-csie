/*
se citesc nr pana se intalneste 4
sa se calc media aritmetica
*/

#include <stdio.h>

int main()
{
    int k, n = 0, s = 0;

    printf("Introduceti numerele: ");

    while (n != 4)
    {
        scanf("%d", &n);

        k++;
        s += n;
    }

    printf("\nMedia este: %.3f \n", (float) s / k);
}
