/*
se citeste o matrice m*n
sa se stearga toate coloanele care incep cu "1"
*/

#include <stdio.h>

using namespace std;

int main()
{
    int i, j, n, m, k;
    int x[50][50], y[50][50];
    printf("Introduceti nr de linii: ");
    scanf("%d", &m);
    printf("Introduceti nr de coloane: ");
    scanf("%d", &n);
    for (i = 0; i < m; i++)
    {
        for (j = 0; j < n; j++)
        {
            printf("Introduceti elementul de pe pozitia (%d,%d): ", i + 1, j + 1);
            scanf("%d", &x[i][j]);
        }
    }

    k = -1;
    for (i = 0; i < n; i++)
    {
        if (x[0][i] != 1)
        {
            k++;
            for (j = 0; j < m; j++)
            {
                y[j][k] = x[j][i];
            }
        }
    }

    printf("Matricea rezultata este: \n");

    for (i = 0; i < m; i++)
    {
        for (j = 0; j <= k; j++)
        {
            printf("%d\t", y[i][j]);
        }
        printf("\n");
    }

    return 0;
}

