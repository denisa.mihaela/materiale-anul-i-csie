/*
suma elementelor prime dintr-o matrice
*/

#include <stdio.h>
#include <math.h>

using namespace std;

int main()
{
    int i, j, m, n;
    int x[50][50];
    printf("Introduceti numarul de linii si de coloane: ");
    scanf("%d %d", &m, &n);
    for (i = 0; i < m; i++)
    {
        for (j = 0; j < n; j++)
        {
            printf("Introduceti elementul de pe pozitia (%d,%d): ", i + 1, j + 1);
            scanf("%d", &x[i][j]);
        }
    }

    int s = 0;
    int k, prim;

    for (i = 0; i < m; i++)
    {
        for (j = 0; j < n; j++)
        {
            prim = 1;
            for (k = 2; k <= ceil(x[i][j] / 2); k++)
            {
                if (x[i][j] % k == 0) prim = 0;
            }
            if (prim) s+= x[i][j];
        }
    }

    printf("Suma elementelor prime = %d \n", s);

    return 0;
}
