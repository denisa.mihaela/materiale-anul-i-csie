/*
Transpura unei matrici patratice
*/

#include <stdio.h>

using namespace std;

int main()
{
    int i, j, n;
    int x[50][50];
    printf("Introduceti dimensiunea matricii: ");
    scanf("%d", &n);
    for (i = 0; i < n; i++)
    {
        for (j = 0; j < n; j++)
        {
            printf("Introduceti elementul de pe pozitia (%d,%d): ", i + 1, j + 1);
            scanf("%d", &x[i][j]);
        }
    }

    int t;

    for (i = 0; i < n - 1; i++)
    {
        for (j = i + 1; j < n; j++)
        {
            t = x[i][j];
            x[i][j] = x[j][i];
            x[j][i] = t;
        }
    }

    printf("Matricea transpusa este: \n");

    for (i = 0; i < n; i++)
    {
        for (j = 0; j < n; j++)
        {
            printf("%d\t", x[i][j]);
        }
        printf("\n");
    }

    return 0;
}
