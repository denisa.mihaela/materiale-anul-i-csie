/*
n factorial
*/

#include <stdio.h>

using namespace std;

int fact(int n)
{
    if (n <= 1) return n;

    int k = n * fact(n - 1);

    return k;
}

int main()
{
    int n;

    printf("Introduceti numarul n: ");
    scanf("%d", &n);

    printf("n factorial = %d", fact(n));

    return 0;
}
