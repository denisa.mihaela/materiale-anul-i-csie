/*
sa se ordoneze crescator elementele pare ale unui vector si sa se mute in fata toate aceste elemente
*/

#include <stdio.h>

using namespace std;

int main()
{
    int i, j, n;
    int x[50];
    printf("Introduceti dimensiunea vectorului: ");
    scanf("%d", &n);
    for (i = 0; i < n; i++)
    {
        printf("Introduceti elementul de pe pozitia (%d): ", i + 1);
        scanf("%d", &x[i]);
    }

    int t;

    for (i = 0; i < n - 1; i++)
    {
        for (j = i + 1; j < n; j++)
        {
            if (x[i] % 2 != 0 && x[j] % 2 == 0)
            {
                t = x[i];
                x[i] = x[j];
                x[j] = t;
            }
        }
    }

    i = 0;
    while (i < n - 1 && x[i] % 2 == 0)
    {
        j = i + 1;
        while (j < n && x[j] % 2 == 0)
        {
            if (x[i] > x[j])
            {
                t = x[i];
                x[i] = x[j];
                x[j] = t;
            }
            j++;
        }
        i++;
    }

    printf("Vectorul rezultat este: ");
    for (i = 0; i < n; i++)
    {
        printf("%d ", x[i]);
    }

    return 0;
}
