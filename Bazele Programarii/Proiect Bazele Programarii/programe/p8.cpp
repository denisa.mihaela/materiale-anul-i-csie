/*
sa se citeasca un string (numele unei persoane)
sa se pastreze doar literele
prima litera sa devina majuscula
*/

#include <stdio.h>
#include <string.h>

using namespace std;

int main()
{
    char s[100], t[100];
    int i, k = 0, c;

    printf("Introduceti numele: ");
    scanf("%[^\n]", &s);

    for (i = 0; i < strlen(s); i++)
    {
        c = (int) s[i];
        if ((c == 32 && k > 0) || (c >= 65 && c <= 90) || (c >= 97 && c <= 122))
        {
            if (k == 0)
            {
                t[k] = (c >= 65 && c <= 90) ? (char) c : (char) (c - 32);
            }
            else
            {
                t[k] = (char) c;
            }
            k++;
        }
    }
    t[k] = '\0';

    printf("Numele resultat = %s \n", t);

    return 0;
}

