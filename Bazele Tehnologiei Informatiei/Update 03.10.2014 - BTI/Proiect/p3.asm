segm_stiva        SEGMENT 

            stiva      DB 128 DUP (?)

segm_stiva        ENDS

 

segm_date        SEGMENT 

            a DB 23
            b DB  7
            c DB 13 
            e DB  ?

segm_date        ENDS

 

segm_cod         SEGMENT 

            ASSUME CS:segm_cod, DS:segm_date, SS:segm_stiva

            et:        MOV AX, segm_date

                        MOV DS, AX


                        MOV     AL, a

                        SUB     AL, b
			
                        ADD     AL, c
					
                        MOV     e, AL

 

                        MOV AX, 4C00H

                        INT 21H

segm_cod         ENDS

END et

