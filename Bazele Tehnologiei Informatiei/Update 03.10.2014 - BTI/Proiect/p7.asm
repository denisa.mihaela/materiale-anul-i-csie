  
   
 s1 segment 
      rez dw ?
      vector dw 4 dup (1,5,2,7)
	
 s1 ends
 s2 segment 
       assume cs:s2,ds:s1
 start:
       mov ax,s1
       mov ds,ax

       mov si,offset vector
       mov cx,size vector+ offset vector
       mov ax,0
 reia:
	add ax, [si]
	add si,type vector
        cmp si,cx
	jl reia
	mov rez,ax

        mov ax,4c00h
        int 21h
 s2 ends
    end start
